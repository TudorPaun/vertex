import PyInstaller.__main__
import os


PyInstaller.__main__.run([
    '--clean',
    '--name=verTex',
    '--onefile',
    '--hidden-import=sqlite3',
    '--windowed',
    '--noconsole',
    # '--log-level=DEBUG',
    '--add-binary=vertex_icon.png;.',
    # '--add-data=vertex_icon.png;.',
    '--icon=vertex_icon2.ico',

    # 'test.py',
    'main.py',
])