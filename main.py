import sys
import sqlite3
import re
import os
from datetime import date
from PyQt5 import QtWidgets, QtCore, QtGui, sip

class TextWindow(QtWidgets.QTextEdit):
    def __init__(self):
        super(TextWindow, self).__init__()
        self.setFont(QtGui.QFont('Arial', 20))
        self.update_wordcount()

    def keyPressEvent(self, event):
        self.update_wordcount()
        self.setTextColor(QtGui.QColor("blue"))
        super(TextWindow, self).keyPressEvent(event)

    def update_wordcount(self):
        text = self.toPlainText()
        self.character_count = len(text)
        self.word_count = len(text.split())

    def insertFromMimeData(self, source):
        text = source.text()
        self.setTextColor(QtGui.QColor("blue"))
        self.insertPlainText(text)

    def mousePressEvent(self, event):
        pos = event.pos()
        cursor = self.cursorForPosition(pos)
        cursor.setPosition(cursor.position())
        self.setTextCursor(cursor)
        super(TextWindow, self).mousePressEvent(event)


class Find(QtWidgets.QDialog):
    def __init__(self, parent = None):
        super(Find, self).__init__(parent)
        self.parent = parent
        self.lastMatch = None

        findButton = QtWidgets.QPushButton("Find",self)
        findButton.clicked.connect(self.find)
        replaceButton = QtWidgets.QPushButton("Replace",self)
        replaceButton.clicked.connect(self.replace)
        allButton = QtWidgets.QPushButton("Replace all",self)
        allButton.clicked.connect(self.replaceAll)
        self.findField = QtWidgets.QTextEdit(self)
        self.findField.resize(250,50)
        self.replaceField = QtWidgets.QTextEdit(self)
        self.replaceField.resize(250,50)

        layout = QtWidgets.QGridLayout()
        layout.addWidget(self.findField,1,0,1,4)
        layout.addWidget(findButton,2,0,1,4)
        layout.addWidget(self.replaceField,3,0,1,4)
        layout.addWidget(replaceButton,4,0,1,2)
        layout.addWidget(allButton,4,2,1,2)
        self.setGeometry(300,300,360,250)
        self.setWindowTitle("Find and Replace")
        self.setLayout(layout)

    def find(self):
        text = self.parent.text.toPlainText()
        query = self.findField.toPlainText()
        start = self.lastMatch.start() + 1 if self.lastMatch else 0
        pattern = re.compile(query,0)
        self.lastMatch = pattern.search(text,start)
        if self.lastMatch:
            start = self.lastMatch.start()
            end = self.lastMatch.end()
            self.moveCursor(start,end)
        else:
            self.parent.text.moveCursor(QtGui.QTextCursor.End)
        
    def replace(self):
        cursor = self.parent.text.textCursor()
        if self.lastMatch and cursor.hasSelection():
            self.parent.text.setTextColor(QtGui.QColor("blue"))
            cursor.insertText(self.replaceField.toPlainText())
            self.parent.text.setTextCursor(cursor)

    def replaceAll(self):
        self.lastMatch = None
        self.find()
        while self.lastMatch:
            self.replace()
            self.find()

    def moveCursor(self,start,end):
        cursor = self.parent.text.textCursor()
        cursor.setPosition(start)
        cursor.movePosition(QtGui.QTextCursor.Right,QtGui.QTextCursor.KeepAnchor,end - start)
        self.parent.text.setTextCursor(cursor)


class Window(QtWidgets.QMainWindow):

    def __init__(self):
        super(Window, self).__init__()
        self.revision_name = ''
        self.changesSaved = True
        self.refresh_changed = False
        self.uncomitted_revision_id = None
        self.current_revision_id = None

        self.setWindowTitle("verTex")
        if getattr(sys, 'frozen', False):
            resource_path = sys._MEIPASS
        else:
            resource_path = os.path.dirname(os.path.abspath(__file__))
        self.setWindowIcon(QtGui.QIcon(resource_path+"/vertex_icon.png"))
        self.setAcceptDrops(True)
        self.setGeometry(0,0, 300,500)
        self.setSize(factor=0.6)
        self.center()

        self.createInterface()

        self.text.setDisabled(True)
        self.text.cursorPositionChanged.connect(self.cursorPosition)
        self.text.textChanged.connect(self.changed)
        self.history.itemActivated.connect(self.onItemClicked)

        self.show()

    # INTERFACE CREATION
    def createInterface(self):
        main_splitter = QtWidgets.QSplitter(self)
        main_splitter.setOrientation(QtCore.Qt.Horizontal)
        
        self.history = QtWidgets.QTreeWidget()
        self.history.setColumnCount(3)
        self.history.resize(200,0)
        self.history.setFrameShape(QtWidgets.QFrame.StyledPanel)

        self.text = TextWindow()
        # self.text = QtWidgets.QTextEdit(self)
        # self.text.setFont(QtGui.QFont('Arial', 20))
        
        main_splitter.addWidget(self.history)
        main_splitter.addWidget(self.text)
        # do not resize splitter when window changes size
        main_splitter.setStretchFactor(0,0)
        main_splitter.setStretchFactor(1,1)
        self.setCentralWidget(main_splitter)

        self.createMenu()
        self.statusBar = QtWidgets.QStatusBar()
        self.setStatusBar(self.statusBar)
        
    def createMenu(self):
        self.mainMenu = self.menuBar()

        fileMenu = self.mainMenu.addMenu('&File')
        newFileAction = QtWidgets.QAction("&New", self)
        newFileAction.setShortcut("Ctrl+N")
        newFileAction.triggered.connect(self.newFile)
        openFileAction = QtWidgets.QAction("&Open", self)
        openFileAction.triggered.connect(self.openFile)
        saveProgressAction = QtWidgets.QAction("&Save", self)
        saveProgressAction.setShortcut("Ctrl+S")
        saveProgressAction.triggered.connect(self.saveProgress)
        exportFileAction = QtWidgets.QAction("&Export File", self)
        exportFileAction.setShortcut("Ctrl+E")
        exportFileAction.setStatusTip('Export text file')
        exportFileAction.triggered.connect(self.exportFile)
        exitAction = QtWidgets.QAction("&Exit", self)
        exitAction.triggered.connect(self.closeProgram)
        fileMenu.addAction(newFileAction)
        fileMenu.addAction(openFileAction)
        fileMenu.addAction(saveProgressAction)
        fileMenu.addAction(exportFileAction)
        fileMenu.addAction(exitAction)

        actionMenu = self.mainMenu.addMenu('&Actions')
        saveRevisionAction = QtWidgets.QAction("&Save revision", self)
        saveRevisionAction.setShortcut("Ctrl+D")
        saveRevisionAction.triggered.connect(self.saveRevision)
        findAction = QtWidgets.QAction("&Find and Replace", self)
        findAction.setShortcut("Ctrl+F")
        findAction.triggered.connect(self.findInText)
        clearFileAction = QtWidgets.QAction("&Clear File", self)
        clearFileAction.triggered.connect(self.clearFile)
        actionMenu.addAction(saveRevisionAction)
        actionMenu.addAction(findAction)
        actionMenu.addAction(clearFileAction)

        helpMenu = self.mainMenu.addMenu('&Help')
        showHelpAction = QtWidgets.QAction("&Help", self)
        showHelpAction.triggered.connect(self.showHelp)
        showAboutAction = QtWidgets.QAction("&About", self)
        showAboutAction.triggered.connect(self.showAbout)
        helpMenu.addAction(showAboutAction)
        helpMenu.addAction(showHelpAction)

    # MENU ACTIONS
    def newFile(self):
        self.uncomitted_revision_id = None
        self.current_revision_id = None
        if hasattr(self, 'conn'):
            self.conn.close()
        self.filename = QtWidgets.QFileDialog.getSaveFileName(self, "Open", "", "All Files (*.vtx)")[0]
        if self.filename:
            self.file_name = self.filename.split('/')[-1]
            self.setWindowTitle("verTex - " + self.file_name)
            self.file_name =  self.file_name.split('.')[0]
            self.conn = sqlite3.connect(self.filename)
            self.dbcursor = self.conn.cursor()
            self.dbcursor.execute('''CREATE TABLE revision
                (id integer PRIMARY KEY, revision_name text, revision_text text, date text)''')
            self.create_new_revision_from_last_commit('')
            self.loadRevisions()
            self.openUnmergedRevision()
            self.text.setDisabled(False)

    def openFile(self):
        self.uncomitted_revision_id = None
        self.current_revision_id = None
        if hasattr(self, 'conn'):
            self.conn.close()
        self.filename = QtWidgets.QFileDialog.getOpenFileName(self, 'Open File', '', 'Text Files (*.vtx)', )[0]
        if self.filename:
            self.file_name = self.filename.split('/')[-1]
            self.setWindowTitle("verTex - " + self.file_name)
            self.file_name =  self.file_name.split('.')[0]
            self.conn = sqlite3.connect(self.filename)
            self.dbcursor = self.conn.cursor()
            self.loadRevisions()
            self.openUnmergedRevision()
            self.text.setDisabled(False)

    def saveProgress(self):
        item = self.history.topLevelItem(0)
        if item:
            item.setText(0, item.text(0).replace("*", ""))
            self.changesSaved = True
            self.dbcursor.execute("UPDATE revision SET revision_text=? WHERE id=" + self.uncomitted_revision_id,  (self.text.toHtml(),))
            self.conn.commit()

    def exportFile(self):
        text = self.text.toPlainText()
        if text:
            name = self.file_name + ' - ' + self.current_revision_id
            if self.revision_name:
                name += ' - ' + self.revision_name
            if self.uncomitted_revision_id == self.current_revision_id:
                self.saveProgress()
            with open(name + '.txt', 'w') as f:
                f.write(text)

    def saveRevision(self):
        if self.uncomitted_revision_id:
            revision_name, ok = QtWidgets.QInputDialog.getText(self, "Revision name", "Write a name for the revision")
            if ok:
                txt = self.text.toHtml()
                self.dbcursor.execute("UPDATE revision SET revision_name=?, revision_text=?, date=? WHERE id=" + str(self.uncomitted_revision_id),  (revision_name, txt, date.today().strftime("%b/%d/%Y")))
                self.conn.commit()
                self.create_new_revision_from_last_commit(self.text.toPlainText())
                self.loadRevisions()
                self.openUnmergedRevision()

    def closeProgram(self):
        self.saveProgress()
        self.close()

    def findInText(self):
        self.find = Find(parent=self)
        self.find.show()

    def clearFile(self):
        if self.uncomitted_revision_id:
            ret = QtWidgets.QMessageBox.question(self, '', "This will clear everything from the file. Are you sure??", QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
            if ret == QtWidgets.QMessageBox.Yes:
                sql = 'DELETE FROM revision'
                self.dbcursor.execute(sql)
                self.conn.commit()
                self.create_new_revision_from_last_commit('')
                self.loadRevisions()
                self.openUnmergedRevision()

    def showAbout(self):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Information)

        msg.setWindowTitle("vertex")
        msg.setText("About vertex")
        msg.setInformativeText("Version: 1.0.0\nCommit: xxx")
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        msg.exec_()
    
    def showHelp(self):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.NoIcon)

        msg.setWindowTitle("Help")
        msg.setText("verTex help")
        instruction_text = "vetTex is a notepad that keeps track of its own versions. Blue text is new text that has been added to this revision, black text is text that existed from the previous revision. \n\nOnce saved, a revision can no longer be edited."
        shortcut_list = " - Ctrl+N: Start a new file.\n - Ctrl+S: Save current progress without creating a new rvision.\n - Ctrl+D: Create a new revision and save the current text into it.\n - Ctrl+E: Export the current revision to a text file.\n - Ctrl+F: Open the Search and Replace dialogue.\n - Ctrl+C: Copy\n - Ctrl+X: Cut\n - Ctrl+V: Paste\n - Ctrl+Z: Undo"
        msg.setInformativeText(instruction_text + "\n\nList of shortcuts:\n" + shortcut_list)
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        msg.exec_()
    
    # EVENTS
    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        self.uncomitted_revision_id = None
        self.current_revision_id = None
        if hasattr(self, 'conn'):
            self.conn.close()
        data = event.mimeData().urls()
        self.filename = data[0].path()
        self.file_name = self.filename.split('/')[-1]
        self.setWindowTitle("verTex - " + self.file_name)
        self.file_name =  self.file_name.split('.')[0]
        self.conn = sqlite3.connect(self.filename)
        self.dbcursor = self.conn.cursor()
        self.loadRevisions()
        self.openUnmergedRevision()
        self.text.setDisabled(False)

    @QtCore.pyqtSlot(QtWidgets.QTreeWidgetItem, int)
    def onItemClicked(self, it, col):
        if self.uncomitted_revision_id == self.current_revision_id:
            self.saveProgress()
        self.current_revision_id = it.text(0)
        self.dbcursor.execute('SELECT revision_name, revision_text, date FROM revision WHERE id=' + str(self.current_revision_id))
        aa = self.dbcursor.fetchall()
        self.refresh_changed = False
        self.text.setHtml(aa[0][1])
        self.revision_name = aa[0][0]
        if aa[0][2] != '---':
            self.text.setReadOnly(True)
        else:
            self.text.setReadOnly(False)

    def changed(self):
        if self.uncomitted_revision_id == self.current_revision_id:
            if self.refresh_changed:
                self.changesSaved = False
                item = self.history.topLevelItem(0)
                if not "*" in item.text(0):
                    item.setText(0, "*" + item.text(0))
            self.refresh_changed = True
        
    def cursorPosition(self):
        cursor = self.text.textCursor()
        # Mortals like 1-indexed things
        line = cursor.blockNumber() + 1
        col = cursor.columnNumber()
        self.statusBar.showMessage("Line: {} | Column: {}          Characters: {} | Words: {}".format(line, col, self.text.character_count, self.text.word_count))

    # OTHER FUNCTIONS
    def create_new_revision_from_last_commit(self, plain_text):
        hidden_text = TextWindow()
        hidden_text.setTextColor(QtGui.QColor("black"))
        hidden_text.setPlainText(plain_text)
        plain_text = hidden_text.toHtml()
        self.dbcursor.execute("INSERT INTO revision (revision_text, date) VALUES (?,?)",  (plain_text,'---',))
        self.conn.commit()

    def openUnmergedRevision(self):
        item = self.history.topLevelItem(0)
        self.onItemClicked(item, 0)  # to trigger the selection logic
        item.setSelected(True)  # to move the highlight in the list

    def center(self):
        frameGm = self.frameGeometry()
        screen = QtWidgets.QApplication.desktop().screenNumber(QtWidgets.QApplication.desktop().cursor().pos())
        centerPoint = QtWidgets.QApplication.desktop().screenGeometry(screen).center()
        frameGm.moveCenter(centerPoint)
        self.move(frameGm.topLeft())

    def setSize(self, factor):
        if factor>1:
            factor = 0.6
        screen = QtWidgets.QApplication.desktop().screenNumber(QtWidgets.QApplication.desktop().cursor().pos())
        size = QtWidgets.QApplication.desktop().screenGeometry(screen).size()*factor
        self.resize(size)

    def loadRevisions(self):
        self.history.clear()
        items = []
        self.dbcursor.execute('SELECT COUNT(*) FROM revision')
        count = self.dbcursor.fetchall()[0][0]
        for row in self.dbcursor.execute('SELECT id,revision_name,date FROM revision ORDER BY -id'):
            items.append(QtWidgets.QTreeWidgetItem(self.history, [str(row[0]), row[1], row[2]]))
        if len(items)==0:
            items.append(QtWidgets.QTreeWidgetItem(self.history, [str(count+1), 'current workspace', '---']))
        self.uncomitted_revision_id = self.history.topLevelItem(0).text(0)


app = QtWidgets.QApplication(sys.argv)
GUI = Window()
sys.exit(app.exec_())